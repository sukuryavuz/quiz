package pt.ipp.isep.dei.examples.tdd.basic.ui;

import pt.ipp.isep.dei.examples.tdd.basic.domain.Calculator;
import pt.ipp.isep.dei.examples.tdd.basic.domain.Quiz;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Quiz quiz = new Quiz();

        Scanner scanner = new Scanner(System.in);
        System.out.println("f = forward, r = rotate, p = paint");
        String input = scanner.nextLine();

        for(int i = 0; i < input.length(); i++) {
            switch(input.charAt(i)){
                case 'f':
                    quiz.moveForward();
                    break;
                case 'p':
                    quiz.paintCell();
                    break;
                case 'r':
                    quiz.rotate();
                    break;
                default:
                    System.out.println("Error wrong character");
            }
        }
        quiz.printField();
        quiz.runInstruction(input);
    }
}