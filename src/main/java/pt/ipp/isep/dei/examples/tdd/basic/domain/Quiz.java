package pt.ipp.isep.dei.examples.tdd.basic.domain;

public class Quiz {
    private Integer direction = 0;
    private Integer[][] field = new Integer[6][6];
    private Integer xPos = 0;
    private Integer yPos = 0;

    public Quiz() {
        for(int i=0; i<6; i++){
            for(int j=0; j<6; j++){
                field[i][j] = 0;
            }
        }
    }

    public void paintCell() {
        try {
            field[xPos][yPos] = 1;
        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            System.out.println("Out of Field");
        }
    }

    public void moveForward(){
        switch(direction){
            case 0:
                yPos++;
                break;
            case 1:
                xPos++;
                break;
            case 2:
                yPos--;
                break;
            case 3:
                xPos--;
                break;
        }
    }

    public void rotate() {
        direction = (direction+1) % 4;
    }

    public void printField(){
        for(int i=0; i<6; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(field[i][j]);
                System.out.print(" | ");
            }
            System.out.println("");
            System.out.println("_______________________");
        }
    }

    public void runInstruction(String input){
        for(int i = 0; i < input.length(); i++){
            switch (input.charAt(i)){
                case 'f':
                    moveForward();
                    break;
                case 'r':
                    rotate();
                    break;
                case 'p':
                    paintCell();
                    break;
                default:
                    System.out.println("Error: Wrong Charakter");
            }
        }
    }

    public Integer[][] returnField(){
        return field;
    }
}