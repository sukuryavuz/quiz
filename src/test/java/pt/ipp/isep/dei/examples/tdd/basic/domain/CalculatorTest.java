package pt.ipp.isep.dei.examples.tdd.basic.domain;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class CalculatorTest {

    @BeforeAll
    public static void classSetUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a CalculatorTest class method and takes place before any @Test is executed");
    }

    @AfterAll
    public static void classTearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a CalculatorTest class method and takes place after all @Test are executed");
    }

    @BeforeEach
    public void setUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place before each @Test is executed");
    }

    @AfterEach
    public void tearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place after each @Test is executed");
    }

    @Test
    @Disabled
    public void failingTest() {
        fail("a disabled failing test");
    }

    @Test
    public void AllFieldSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Integer[][] expectedResult = new Integer[6][6];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                expectedResult[i][j] = 1;
            }
        }
        Quiz quiz = new Quiz();
        quiz.runInstruction("pfpfpfpfpfprfprfpfpfpfpfprrrfprrrfpfpfpfpfprfprfpfpfpfpfprrrfprrrfpfpfpfpfprfprfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void FirstColumnAndLastRowSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Integer[][] expectedResult = new Integer[6][6];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                expectedResult[i][j] = 0;
            }
        }

        expectedResult[5][1] = 1;
        expectedResult[5][2] = 1;
        expectedResult[5][3] = 1;
        expectedResult[5][4] = 1;
        expectedResult[5][5] = 1;

        expectedResult[0][0] = 1;
        expectedResult[1][0] = 1;
        expectedResult[2][0] = 1;
        expectedResult[3][0] = 1;
        expectedResult[4][0] = 1;
        expectedResult[5][0] = 1;


        Quiz quiz = new Quiz();
        quiz.runInstruction("prfpfpfpfpfprrrfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void AllAroundSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Integer[][] expectedResult = new Integer[6][6];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                expectedResult[i][j] = 0;
            }
        }

        expectedResult[5][1] = 1;
        expectedResult[5][2] = 1;
        expectedResult[5][3] = 1;
        expectedResult[5][4] = 1;
        expectedResult[5][5] = 1;

        expectedResult[0][0] = 1;
        expectedResult[1][0] = 1;
        expectedResult[2][0] = 1;
        expectedResult[3][0] = 1;
        expectedResult[4][0] = 1;
        expectedResult[5][0] = 1;

        expectedResult[4][5] = 1;
        expectedResult[3][5] = 1;
        expectedResult[2][5] = 1;
        expectedResult[1][5] = 1;
        expectedResult[0][5] = 1;

        expectedResult[0][1] = 1;
        expectedResult[0][2] = 1;
        expectedResult[0][3] = 1;
        expectedResult[0][4] = 1;



        Quiz quiz = new Quiz();
        quiz.runInstruction("prfpfpfpfpfprrrfpfpfpfpfprrrfpfpfpfpfprrrfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void FirstRowAndLastColoumnSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Integer[][] expectedResult = new Integer[6][6];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                expectedResult[i][j] = 0;
            }
        }
        expectedResult[0][0] = 1;
        expectedResult[0][1] = 1;
        expectedResult[0][2] = 1;
        expectedResult[0][3] = 1;
        expectedResult[0][4] = 1;
        expectedResult[0][5] = 1;

        expectedResult[1][5] = 1;
        expectedResult[2][5] = 1;
        expectedResult[3][5] = 1;
        expectedResult[4][5] = 1;
        expectedResult[5][5] = 1;


        Quiz quiz = new Quiz();
        quiz.runInstruction("pfpfpfpfpfprfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }


}



